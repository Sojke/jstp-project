# README #

This is the README for the project of sojke caluwé for the Jumpstart Program by TTL.be.

This README consists of a short summary of what the project consists of, what it should do and how it needs to get set up.

### What is the purpose of this project? ###

This project consists of a test script to automate the testing of music player and search funcions on soundcloud.com.
The project has at this moment 6 features to test the most important functions of soundcloud.

### What does this repository contain? ###

* A features folder that contains the features, an environment.py file and the steps folder where the file (soundcloud.py) with the code is located
* A Chrome Profile folder that contains a Google Chrome Profile that is used in the tests
* A gitignore file
* This README file

### How do I get set up? ###

* Install Python (make sure you also have pip installed)
* Put your Python folder in 'path' under 'system environment variables'
* Use pip to install Selenium for Python ('pip install selenium' in cmd or powershell)
* Use pip to install Behave ('pip install behave' in cmd or powershell)
* Download a driver for the browser you want to use (e.g. ChromeDriver for Google Chrome)
* Put the driver in 'path' under 'system environment variables'
* Install an Integrated Development Environment that can be used to code in Python (e.g. Visual Studio Code)
* Install the Python extension for your IDE
* Install a Cucumber (Gherkin) Full Support extension for your IDE
* Open the project folder in the IDE and run soundcloud.py with Behave as module