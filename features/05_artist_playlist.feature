Feature: search for a playlist of an artist and play and repost the playlist

Scenario: searh for playlist, play and repost it
Given I am on soundcloud
When I search for "lil skies" under playlists and select playlist number "2"
When I play the playlist and repost it
When I play the next song
When I go to reposts under profile
Then I should see the playlist I reposted and the next song playing