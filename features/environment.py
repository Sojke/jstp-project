from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from data import data


def before_all(context):
	opts = webdriver.ChromeOptions()
	opts.add_argument('no-sandbox')
	#sets a folder where the google chrome account is located that chromedriver will use
	opts.add_argument('user-data-dir=C:\\Users\\User\\Desktop\\Werk\\TTL\\Jumpstart Program\\JSTP test project\\Test Project\\Chrome Profile') #copy of C:\\Users\\User\\AppData\\Local\\Google\\Chrome\\User Data
	#indicates which profile in the folder chromedriver should use
	opts.add_argument('--profile-directory=Profile 2') #tester profile
	#maximizes the chromedriver window when the script starts running
	opts.add_argument("--start-maximized")
	context.browser = webdriver.Chrome(chrome_options=opts)
	context.action = ActionChains(context.browser)

def after_all(context):
	
	context.browser.quit()
	

	
