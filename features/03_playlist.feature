Feature: making a playlist and play the playlist

Scenario: make a playlist and play it
Given I am on soundcloud
When I search for "goldlink zulu screams" and select song number "1"
When I add the song to a playlist
When I create the playlist "Hiphop" and set it "private"
When I save the song in the new playlist
When I add "arizona zervas roxanne", number "1" to my "Hiphop" playlist
When I go to my playlists and select the playlist I made
Then I should see my songs in the playlist