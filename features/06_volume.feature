Feature: search for a song, play the song and use the volume slider to lower the volume

Scenario: searh for song, play it and lower volume
Given I am on soundcloud
When I search for "jack harlow ghost" and select song number "1"
When I play the song
When I mute the song
When I turn the volume up to "30"
Then I should see that the volume is no longer muted