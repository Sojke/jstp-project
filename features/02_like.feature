Feature: liking a song on soundcloud and playing the liked song

Scenario: liking a song and going to liked
Given I am on soundcloud
When I search for "roddy ricch the box" and select song number "1"
When I like the song
When I navigate to liked songs
When I select the song I just liked
When I play the song
When I unlike the song
Then I should see the song in the music player and not under likes