Feature: going to soundcloud, add a song to play next and play the next song

Scenario: add a song to play next and play it
Given I am on soundcloud
When I search for "goldlink joke ting" via the searchbar
When I select song number "1"
When I play the song
When I search for "kaytranada 10%" and select song number "1"
When I add a song to the queue
When I play the next song
Then I should see the next song in the music player