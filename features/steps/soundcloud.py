
import time
#imported to use the search function
import re
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

#checks for commercials by looping a sleep of 1s while the next or previous buttons are disabled
def check_for_commercials(context):
    context.nextButton = context.browser.find_element(By.CSS_SELECTOR, "button.skipControl__next")
    context.previousButton = context.browser.find_element(By.CSS_SELECTOR, "button.skipControl__previous")
    while 'disabled' in context.nextButton.get_attribute('class').split() or 'disabled' in context.previousButton.get_attribute('class').split():
        time.sleep(1)



@given('I am on soundcloud')
def go_to_soundcloud(context):
    urlSoundcloud = context.browser.current_url
    #checks if the url contains soundcloud.com if not the webdriver goes to the soundcloud homepage
    urlCheck = re.search("^https://soundcloud.com", urlSoundcloud)
    if not (urlCheck):
        context.browser.get("https://soundcloud.com/discover")
    
    #sets songpPlaying to 0 every time a new feature starts so when a function is used to select the next song this variable can be increased to check which song is playing later on
    context.songPlaying = 0 #0 so the first song playing is also the first index of a list

    
@when('I search for "{song}" via the searchbar')
def use_search(context, song):
    time.sleep(1)
    searchHeader = context.browser.find_element(By.CSS_SELECTOR, ".header__search input[name='q']")
    searchHeader.clear()
    searchHeader.send_keys(song)
    submitHeader = context.browser.find_element(By.CSS_SELECTOR, ".header__search button[type='submit']")
    submitHeader.click()

@when('I select song number "{number}"')
def select_song(context, number):
    time.sleep(1)
    context.songList = context.browser.find_elements(By.CSS_SELECTOR, "ul a.sound__coverArt")
    songIcon = context.songList[(int(number)-1)]
    #makes sure the selected song is in the view so it can be clicked
    songIcon.location_once_scrolled_into_view
    songIcon.click()
    time.sleep(1)

@when('I play the song')
def use_play_button(context):
    check_for_commercials(context)
    songPlay = context.browser.find_element(By.CSS_SELECTOR, "a[Title='Afspelen']")
    songPlay.click()
    time.sleep(1)

@when('I search for "{song}" and select song number "{number}"')
def search_song_header(context, song, number):
    use_search(context, song)
    time.sleep(2) #important!
    songTitleList = context.browser.find_elements(By.CSS_SELECTOR, "li.searchList__item div.sound__content a.soundTitle__title span")
    time.sleep(1) #important!
    #saves the title of the song selected for later use
    context.songTitle = songTitleList[(int(number)-1)].text
    time.sleep(1)
    select_song(context, number)

@when('I add a song to the queue')
def add_song_to_queue(context):
    addQueue = context.browser.find_element(By.CSS_SELECTOR, "button.addToNextUp")
    addQueue.click()
    time.sleep(1)

@when('I play the next song')
def use_next_button(context):
    time.sleep(1)
    check_for_commercials(context)
    context.nextButton.click()
    #adds 1 to songPlaying so it can later be determined which song is playing from a list
    context.songPlaying += 1
    time.sleep(1)

@then('I should see the next song in the music player')
def check_music_player(context):
    musicPlayerTitle = context.browser.find_element(By.CSS_SELECTOR, ".playbackSoundBadge__title span:nth-of-type(2)")
    assert musicPlayerTitle.text == context.songTitle


@when('I like the song')
def like_song(context):
    likeButtonSong = context.browser.find_element(By.CSS_SELECTOR, "button.sc-button-like")
    #makes sure the song only gets liked when it's not already liked so it won't get unliked
    if 'sc-button-selected' not in likeButtonSong.get_attribute('class').split():
        likeButtonSong.click()
    time.sleep(1)

@when('I navigate to liked songs')
def go_to_liked_songs(context):
    context.userMenuButton = context.browser.find_element(By.CSS_SELECTOR, "a[data-test-id='user-nav-btn']")
    context.userMenuButton.click()
    #makes sure userLikesButton is present and clickable
    while True:
        try:
            userLikesButton = context.browser.find_element(By.CSS_SELECTOR, "a[href='/you/likes']")
            userLikesButton.click()
            break
        except:
            pass
    time.sleep(1)

@when('I select the song I just liked')
def select_liked_song(context):
    #puts all the song titles under likes songs in a list
    likedSongs = context.browser.find_elements(By.CSS_SELECTOR, "div.playableTile__descriptionContainer a")
    #checks which title in LikedSongs is the same as the title from the song I selected and later liked
    for songs in likedSongs:
        if songs.text == context.songTitle:
            songNeeded = songs
            break
    time.sleep(1)
    songNeeded.click()

@when('I unlike the song')
def unlike_song(context):
    likeButtonSong = context.browser.find_element(By.CSS_SELECTOR, "button.sc-button-like")
    #makes sure the song only gets unliked when it's liked so it won't get liked again
    if 'sc-button-selected' in likeButtonSong.get_attribute('class').split():
        likeButtonSong.click()
    time.sleep(1)

@then('I should see the song in the music player and not under likes')
def check_unliked_song(context):
    check_music_player(context)
    go_to_liked_songs(context)
    likedSongs = context.browser.find_elements(By.CSS_SELECTOR, "div.playableTile__descriptionContainer a")
    #searches for a song with the same title as the song I selected and later liked
    #if it is not present like it should be, songInList gets set to False
    for songs in likedSongs:
        if songs.text == context.songTitle:
            songInList = True
            break
        else:
            songInList = False
    #checks if the song is not under likes
    assert songInList == False


@when('I add the song to a playlist')
def add_song_to_playlist(context):
    moreButton = context.browser.find_element(By.CSS_SELECTOR, "button.sc-button-more")
    moreButton.click()
    playlistButton = context.browser.find_element(By.CSS_SELECTOR, "button.sc-button-addtoset")
    playlistButton.click()
    time.sleep(1)

@when('I create the playlist "{playlist}" and set it "{privacy}"')
def name_playlist(context, playlist, privacy):
    #makes an empty list to save all the songs in that come in the new playlist
    context.playlistSongs = []
    #puts the selected song in the list
    context.playlistSongs.append(context.songTitle)
    playlistSelector = context.browser.find_element(By.CSS_SELECTOR, "div.tabs div:nth-of-type(1)")
    #if there are already existing playlists the right tab gets selected to create a new playlist
    if playlistSelector.get_attribute("class") == "tabs__tabs":
        makePlaylist=context.browser.find_element(By.CSS_SELECTOR, "div.tabs__tabs li:nth-of-type(2) a")
        makePlaylist.click()
    time.sleep(1)
    nameField = context.browser.find_element(By.CSS_SELECTOR, "div.createPlaylist__title input")
    nameField.send_keys(playlist)
    #the name you have given to the playlist gets saved for later use
    context.playlistName = playlist
    if privacy == "private":
        radioPrivate = context.browser.find_element(By.CSS_SELECTOR, "input[value='private'] ~ div.radioGroup__radioRadio")
        radioPrivate.click()
    time.sleep(1)

@when('I save the song in the new playlist')
def save_playlist(context):
    makeButton = context.browser.find_element(By.CSS_SELECTOR, "button.createPlaylist__saveButton")
    makeButton.click()
    time.sleep(1) #important!
    goBack = context.browser.find_element(By.CSS_SELECTOR, "button.modal__closeButton")
    goBack.click()
    time.sleep(1)

@when('I add "{song}", number "{number}" to my "{playlist}" playlist')
def add_new_song_to_playlist(context, song, number, playlist):
    search_song_header(context, song, number)
    #adds the newly selected song to the list of songs in the playlist if the playlist is the one created earlier
    try:
        if context.playlistName == playlist:
            context.playlistSongs.append(context.songTitle)
    except:
        pass
    add_song_to_playlist(context)
    selectPlaylists = context.browser.find_elements(By.CSS_SELECTOR, "div.addToPlaylistList__list li h3 a")
    #selects the playlist given from the list and puts the index in a variable
    for playlists in selectPlaylists:
        if playlists.text == playlist:
            playlistNumber = selectPlaylists.index(playlists)
            break
    playlistButtons = context.browser.find_elements(By.CSS_SELECTOR, "div.addToPlaylistList__list li button")
    #uses the index saved in playlistNumber to click on the playlist needed
    playlistButtons[playlistNumber].click()
    time.sleep(1)
    goBack = context.browser.find_element(By.CSS_SELECTOR, "button.modal__closeButton")
    goBack.click()

@when('I go to my playlists and select the playlist I made')
def go_to_playlists(context):
    userMenuButton = context.browser.find_element(By.CSS_SELECTOR, "a[data-test-id='user-nav-btn']")
    userMenuButton.click()
    #makes sure userPlaylistsButton is present and clickable
    while True:
        try:
            userPlaylistsButton = context.browser.find_element(By.CSS_SELECTOR, "a[href='/you/sets']")
            userPlaylistsButton.click()
            break
        except:
            pass
    time.sleep(1) #important!
    playlistNames = context.browser.find_elements(By.CSS_SELECTOR, "li.badgeList__item div.playableTile__descriptionContainer a")
    for playlists in playlistNames:
        if playlists.text == context.playlistName:
            playlistNeeded = playlists
            break
    time.sleep(1)
    playlistNeeded.click()
    time.sleep(1)

@then('I should see my songs in the playlist')
def check_for_songs_in_playlist(context):
    playlistTitles = context.browser.find_elements(By.CSS_SELECTOR, "ul.trackList__list li div.trackItem__content a.trackItem__trackTitle")
    for titles in playlistTitles:
       assert titles.text ==  context.playlistSongs[playlistTitles.index(titles)]
    deletePlaylist = context.browser.find_element(By.CSS_SELECTOR, "button.sc-button-delete")
    deletePlaylist.click()
    time.sleep(1)
    confirmButton = context.browser.find_element(By.CSS_SELECTOR, "div.deletePlaylistModal__buttons button[type='submit']")
    confirmButton.click()


@when('I search for "{artist}" under artists and select artist number "{number}"')
def search_and_select_artist(context,artist,number):
    song = artist
    use_search(context,song)
    artistSearch = context.browser.find_element(By.CSS_SELECTOR, "li.searchOptions__navigation-people a")
    artistSearch.click()
    time.sleep(1) #important!
    artistList = context.browser.find_elements(By.CSS_SELECTOR, "ul a.userItem__coverArt")
    artistIcon = artistList[(int(number)-1)]
    artistIcon.location_once_scrolled_into_view
    artistNamesList = context.browser.find_elements(By.CSS_SELECTOR, "h2.userItem__title a")
    context.artistName = artistNamesList[(int(number)-1)].text
    artistIcon.click()
    time.sleep(1)

@when('I follow the artist')
def use_follow_artist(context):
    followButton = context.browser.find_element(By.CSS_SELECTOR, "button.sc-button-follow")
    if 'sc-button-selected' not in followButton.get_attribute('class').split():
        followButton.click()

@when('I go to the artists I follow')
def go_to_following(context):
    userMenuButton = context.browser.find_element(By.CSS_SELECTOR, "a[data-test-id='user-nav-btn']")
    userMenuButton.click()
    #makes sure userFollowingButton is present and clickable
    while True:
        try:
            userFollowingButton = context.browser.find_element(By.CSS_SELECTOR, "a[href='/you/following']")
            userFollowingButton.click()
            break
        except:
            pass

@then('I should see the artist I just followed')
def check_artist_following(context):
    #determines that the artist I followed is by default not under following
    artistInFollowing = False
    time.sleep(1) #important!
    followingArtists = context.browser.find_elements(By.CSS_SELECTOR, "div.userBadgeListItem__title a")
    #searches for the artist I followed in the list of artists under following
    #if the artist I followed is in the list the variable artistInFollowing gets set to true
    for artists in followingArtists:
        if artists.text == context.artistName:
            artistInFollowing = True
            artistNumber = followingArtists.index(artists)
            break
    #checks if the artist I followed is indeed under following
    assert artistInFollowing == True
    followingButtons = context.browser.find_elements(By.CSS_SELECTOR, "div.userBadgeListItem__action button")
    time.sleep(1) #important!
    if 'sc-button-selected' in  followingButtons[artistNumber].get_attribute('class').split():
        followingButtons[artistNumber].click()


@when('I search for "{playlist}" under playlists and select playlist number "{number}"')
def search_and_select_playlist(context,playlist,number):
    song = playlist
    use_search(context,song)
    playlistSearch = context.browser.find_element(By.CSS_SELECTOR, "li.searchOptions__navigation-playlists a")
    playlistSearch.click()
    time.sleep(3) #important!
    playlistList = context.browser.find_elements(By.CSS_SELECTOR, "ul a.sound__coverArt")
    playlistIcon = playlistList[(int(number)-1)]
    playlistIcon.location_once_scrolled_into_view
    playlistNamesList = context.browser.find_elements(By.CSS_SELECTOR, "ul a.soundTitle__title span")
    context.playlistName = playlistNamesList[(int(number)-1)].text
    playlistIcon.click()
    time.sleep(1)

@when('I play the playlist and repost it')
def play_and_repost(context):
    songsInList = context.browser.find_elements(By.CSS_SELECTOR, "div.trackItem__content a")
    #creates an empty list to save all the titles of the songs in the playlist in
    context.songTitles = []
    #puts all the titles of the songs in the playlist in an array that can be used later
    for songs in songsInList:
        context.songTitles.append(songs.text)
    use_play_button(context)
    #this checks for a 'hint' that can pop up
    #if the hint pops up it gets clicked away
    try:
        understoodButton = context.browser.find_element(By.CSS_SELECTOR, "div.callout__button button")
        understoodButton.click()
    except NoSuchElementException:
        pass
    repostButton = context.browser.find_element(By.CSS_SELECTOR, "button.sc-button-repost")
    if 'sc-button-selected' not in repostButton.get_attribute('class').split():
        repostButton.click()
    time.sleep(1)

@when('I go to reposts under profile')
def go_to_reposts(context):
    userMenuButton = context.browser.find_element(By.CSS_SELECTOR, "a[data-test-id='user-nav-btn']")
    userMenuButton.click()
    #makes sure userProfileButton is present and clickable
    while True:
        try:
            userProfileButton = context.browser.find_element(By.CSS_SELECTOR, "a.profileMenu__profile")
            userProfileButton.click()
            break
        except:
            pass
    repostsCategorie = context.browser.find_element(By.CSS_SELECTOR, "ul.profileTabs li:last-of-type a")
    repostsCategorie.click()

@then('I should see the playlist I reposted and the next song playing')
def check_playlist(context):
    context.songTitle = context.songTitles[context.songPlaying]
    check_music_player(context)
    time.sleep(1) #important!
    repostItems = context.browser.find_elements(By.CSS_SELECTOR, "ul.soundList li a.soundTitle__title span")
    underReposts = False
    for reposts in repostItems:
        if reposts.text == context.playlistName:
            underReposts = True
            break
    assert underReposts == True
    repostProfileButton = context.browser.find_element(By.CSS_SELECTOR, "button.sc-button-repost")
    repostProfileButton.location_once_scrolled_into_view
    if 'sc-button-selected' not in repostProfileButton.get_attribute('class').split():
        repostProfileButton.click()


@when('I mute the song')
def use_mute(context):
    muteButton = context.browser.find_element(By.CSS_SELECTOR, "button.volume__button")
    muteVolume = context.browser.find_element(By.CSS_SELECTOR, "div.volume")
    if 'muted' not in muteVolume.get_attribute('class').split():   
        muteButton.click()
    time.sleep(1)

@when('I turn the volume up to "{volume}"')
def lower_volume(context, volume):
    muteButton = context.browser.find_element(By.CSS_SELECTOR, "button.volume__button")
    volumeSlider = context.browser.find_element(By.CSS_SELECTOR, "div.volume__sliderHandle")
    context.action.move_to_element(muteButton).perform()
    context.action.click_and_hold(volumeSlider).move_by_offset(0, -(int(volume)/6*5)).release().perform()
    time.sleep(1)

@then('I should see that the volume is no longer muted')
def check_for_muted(context):
    volumeMuted = True
    muteVolume = context.browser.find_element(By.CSS_SELECTOR, "div.volume")
    if 'muted' not in muteVolume.get_attribute('class').split():   
        volumeMuted = False
    assert volumeMuted == False